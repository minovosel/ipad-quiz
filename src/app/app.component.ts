import { Component, OnDestroy } from '@angular/core';
import { routerTransition } from './router.animations';
import { RouterOutlet, RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ routerTransition ],
})
export class AppComponent{
  title = 'questioneer';
  
  getState(outlet: RouterOutlet): string | null {
    try {
      return outlet.activatedRouteData.state + '/' +outlet.activatedRoute.params['value'].position || '';
    } catch(e) {
      return outlet.activatedRouteData.state;
    }
  }

  onActivate(e) {
    console.log(e.constructor.name);
    if (e.constructor.name === "QuestionPrevComponent" || e.constructor.name === "QuestionComponent" ){
      window.scroll(0,0);
    }
  }
}

export class CustomStrategy extends RouteReuseStrategy {
  shouldDetach(route: ActivatedRouteSnapshot): boolean { return false; }
  store(route: ActivatedRouteSnapshot, detachedTree: DetachedRouteHandle): void {}
  shouldAttach(route: ActivatedRouteSnapshot): boolean { return false; }
  retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle|null { return null; }
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    return false;
  }
}
