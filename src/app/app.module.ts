import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GridComponent } from './grid/grid.component';
import { InfoComponent } from './info/info.component';
import { QuestionComponent } from './question/question.component';
import { SplashComponent } from './splash/splash.component';
import { AppRoutingModule } from './app.routing';
import { RouteReuseStrategy } from '@angular/router';
import { CustomStrategy } from './app.component';
import { AnimationsService } from './router.animation.service';
import { QuestionPrevComponent } from './question/question-prev.component';

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    InfoComponent,
    QuestionComponent,
    QuestionPrevComponent,
    SplashComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [
    {provide: RouteReuseStrategy, useClass: CustomStrategy},
    AnimationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
