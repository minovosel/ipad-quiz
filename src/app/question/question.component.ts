import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppMainService } from '../app-main.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AnimationsService } from '../router.animation.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
  animations: [routerTransition]
})
export class QuestionComponent implements OnInit, OnDestroy {
  category: object;
  questions: object;
  categoryId: number;
  questionPosition: number;
  questionCount: number;

  private swipeCoord?: [number, number];
  private swipeTime?: number;

  constructor(
    private categoryContent: AppMainService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private animationsService: AnimationsService
  ) { 
    this.activatedRoute.params.subscribe((params: Params) => {
      this.categoryId = params.id;
      this.questionPosition = params.position;

      this.categoryContent.contentLoad
        .subscribe(
          (result: object[]) => {
            this.category = result.find( category => +category['id'] === +this.categoryId );
            this.questions = this.category['question_page'][+params.position - 1];
            this.questionCount = this.category['question_page'].length;
          }
        )
    });
  }

  swipe(e: TouchEvent, when: string): void {
    const coord: [number, number] = [e.changedTouches[0].pageX, e.changedTouches[0].pageY];
    const time = new Date().getTime();

    if (when === 'start') {
      this.swipeCoord = coord;
      this.swipeTime = time;
    } else if (when === 'end') {
      const direction = [coord[0] - this.swipeCoord[0], coord[1] - this.swipeCoord[1]];
      const duration = time - this.swipeTime;

      if (duration < 1000 //
        && Math.abs(direction[0]) > 30 // Long enough
        && Math.abs(direction[0]) > Math.abs(direction[1] * 3)) { // Horizontal enough
          const swipe = direction[0] < 0 ? 'next' : 'previous';

          if(swipe === 'next' && this.questionPosition < this.category['question_page'].length){
            const newQuestion = +this.questionPosition + 1;
            this.router.navigate(['/category/' + this.categoryId + '/question/' + newQuestion]);
          }else if(swipe === 'previous' && this.questionPosition > 1){
            const newQuestion = +this.questionPosition - 1;
            this.router.navigate(['/category/' + this.categoryId + '/question/' + newQuestion + '/prev']);
          }
      }
    }
  }


  ngOnDestroy() {
    this.animationsService.updateRouteAnimationType('FADE');
  }

  ngOnInit() {
    const url = this.router.url.split('/');
    if(url[url.length - 1] === 'prev'){
      this.animationsService.updateRouteAnimationType('SLIDE_REVERT');
    }else{
      this.animationsService.updateRouteAnimationType('SLIDE');
    }
  }

}
