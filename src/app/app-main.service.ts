import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppMainService {
  contentLoad: BehaviorSubject<object>;
  content: object[];

  constructor() {
    this.content = [
      {
        id: 1,
        name: 'Åkare',
        color: '#e88e38',
        headline: 'Diskussionskort åkare',
        info: [
          'Korten syftar till att säkra att åkarna är medvetna om svensk konståknings värdegrund samt åkarens ansvar och delaktighet i föreningens klubbkänsla.',
          'Korten lämpar sig bäst för åkarkategori från ungdom och äldre. '
        ],
        question_page: [
          {
            id: 1,
            headline: 'Integration',
            description: 'Det har börjat en ny åkare i din träningsgrupp/ditt lag som inte kan svenska.',
            questions: [
              'Hur hjälper du den här åkaren att känna sig välkommen?',
              'Hur kommunicerar ni?'
            ]
          },
          {
            id: 2,
            headline: 'Hälsa',
            description: 'Du vaknar på morgonen och känner dig febrig. Ikväll är det dags för träning.',
            questions: [
              'Var går din gräns för när du avstår från träning och tävling på grund av sjukdom?',
              'Överensstämmer detta med vad din tränare och dina föräldrar tycker?',
              'Diskuterar ni i gruppen/laget när det är dags att avstå träning pga sjukdom?'
            ]
          },
          {
            id: 3,
            headline: 'Respekt',
            description: 'Din tränares telefon ringer. Tränaren svarar och ägnar några minuter åt samtalet istället för åt träningen. ',
            questions: [
              'Hur reagerar du?',
              'Vad förväntar du dig av din tränare?',
              'Vad gör du för att visa människor i din omgivning respekt?'
            ]
          },
          {
            id: 4,
            headline: 'Relationer',
            description: 'Två personer av samma kön i din träningsgrupp berättar att de är ihop. ',
            questions: [
              'Hur tänker du kring det?',
              'Skulle du reagera på samma sätt oavsett om det var tjejer eller killar?',
              'Hur skapar man ett öppet klimat där alla vågar vara sig själva?'
            ]
          },
          {
            id: 5,
            headline: 'Motivation',
            description: 'Du vet att dina föräldrar vill att du ska lyckas och att de spenderar mycket pengar på din konståkningsträning. Du börjar tappa motivation och känner för att ägna mer tid åt andra intressen. ',
            questions: [
              'Hur pratar du med dina föräldrar om det?',
              'Hur kan du bli mer motiverad att fortsätta?'
            ]
          },
          {
            id: 6,
            headline: 'Varför idrott?',
            description: 'Du har flera klasskompisar som inte sysslar med idrott.',
            questions: [
              'Vad tror du kan vara orsakerna till att de inte idrottar?',
              'Varför har du valt att åka konståkning?',
              'Vad betyder konståkningen för dig?'
            ]
          },
          {
            id: 7,
            headline: 'Sociala medier',
            description: 'En träningskompis skriver ofta nedvärderande saker om andra åkare i sociala medier. ',
            questions: [
              'Vad tycker du om detta?',
              'Hur uppför man sig i sociala medier?'
            ]
          },
          {
            id: 8,
            headline: 'Hälsa',
            description: 'För att orka med sin träning behöver man få i sig ordentligt med allsidig och näringsrik kost.',
            questions: [
              'Pratar ni om vad du behöver äta för att orka träna?',
              'Vad är ett bra mellanmål att ha med till ishallen?'
            ]
          },
          {
            id: 9,
            headline: 'Sociala medier',
            description: 'En av dina kompisar tar bilder både i och utanför omklädningsrummet. Du är orolig för att bilderna ska publiceras.',
            questions: [
              'Vad är viktigt att tänka på innan man publicerar något?',
              'Vad tycker du är okej att publicera?',
              'Vad har ni för regler?'
            ]
          },
          {
            id: 10,
            headline: 'Motivation',
            description: 'En kompis pressas hårt av sina föräldrar. Kompisen tränar redan flera extrapass. På sistone har kompisen börjat få svårt att prestera och verkar inte tycka att det är roligt längre. ',
            questions: [
              'Hur stöttar du din kompis?',
              'Vad kan din kompis göra?',
              'Vem kan vara bra att prata med?'
            ]
          },
          {
            id: 11,
            headline: 'Allas lika värde',
            description: 'En tränare berömmer och lyfter alltid fram samma åkare i gruppen/laget.',
            questions: [
              'Hur pratar du med din tränare om hur du känner?',
              'Är lika alltid rättvist?'
            ]
          },
          {
            id: 12,
            headline: 'Teamkänsla',
            description: 'En åkare i laget begår ett avgörande misstag som gör att ni kommer långt ner i resultatlistan.',
            questions: [
              'Vad gör du för att stötta din lagkamrat?',
              'Vad kan tränaren göra för att stötta?'
            ]
          },
          {
            id: 13,
            headline: 'Uppförande',
            description: 'I ishallen används ofta fula ord i samband med att någon misslyckas eller blir irriterad.',
            questions: [
              'Vilka regler finns i din förening?',
              'Vilket språkbruk tycker du är okej?',
              'Hur hjälps vi åt att skapa ett positivt klimat i ishallen?'
            ]
          },
          {
            id: 14,
            headline: 'Sociala medier',
            description: 'Händer det att du ser olämpliga kommentarer i sociala medier som rör konståkning?',
            questions: [
              'Har du något exempel?',
              'Vad är okej att posta i sociala medier?'
            ]
          },
          {
            id: 15,
            headline: 'Framtidsmöjligheter',
            description: 'En av dina träningskompisar har tröttnat på att tävla och vill trappa ner på träningen. På sikt kan hon tänka sig att bli teknisk funktionär',
            questions: [
              'Vilka träningsalternativ erbjuds i din förening?',
              'Vilka roller finns för den som inte längre vill åka själv?',
              'Till vem vänder du dig?'
            ]
          },
          {
            id: 16,
            headline: 'Antidopningsregler',
            description: 'Jag har diabetes. Vad säger antidopningsreglerna om att jag tar min medicin när jag representerar Sverige på internationella tävlingar?',
            questions: [
              'Kan man söka dispens om man är i behov av medicinering?',
              'Gäller samma regler för åkare oavsett tävlingsnivå?'
            ]
          },
          {
            id: 17,
            headline: 'Trygghet',
            description: 'I din grupp skiljer sig nivån mellan åkarna mycket och vissa är bättre än andra. När tränaren inte märker brukar några åkare ge nedsättande kommentarer och blickar åt andra. ',
            questions: [
              'Hur agerar du?',
              'Hur är ni mot varandra i din grupp?',
              'Hur får vi alla att känna sig välkomna?'
            ]
          },
          {
            id: 18,
            headline: 'Laguttagning',
            description: 'Din tränare toppar alltid laget inför en tävling vilket leder till att alla inte får tävla.',
            questions: [
              'Tycker du att det är okej att toppa laget?',
              'Om ja, i så fall när?'
            ]
          },
          {
            id: 19,
            headline: 'Gemenskap',
            description: 'Du märker att en träningskompis i din grupp/ditt lag är lite utanför gruppen/laget och oftast är för sig själv',
            questions: [
              'Vad kan du göra för att underlätta för den som känner sig utanför?'
            ]
          },
          {
            id: 20,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten.',
            questions: [
              'Vad kan du göra för att bidra till ett sådant klimat?'
            ]
          },
          {
            id: 21,
            headline: 'Respekt',
            description: 'Din mamma är ofta med när du tävlar. Hon är väldigt engagerad och kommenterar det som händer under tävlingen. Det händer att hon kritiserar bedömningen.',
            questions: [
              'Är det okej att ifrågasätta bedömningen?',
              'Hur tycker du att en förälder ska agera under tävling?'
            ]
          },
          {
            id: 22,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten. På isen visar dina klubbkamrater sitt dåliga humör när de inte lyckas eller får sin vilja igenom.',
            questions: [
              'Hur agerar du?',
              'Hur är man en bra träningskompis?',
              'Hur bör man tänka för att sprida positiv energi?'
            ]
          },
          {
            id: 23,
            headline: 'Förebilder',
            description: 'Inom svensk konståkning pratar vi om att vara förebilder och fostra förebilder.',
            questions: [
              'Vad är en bra förebild?',
              'Hur kan du vara en bra förebild för dina klubbkompisar?'
            ]
          },
          {
            id: 24,
            headline: 'Balans i livet',
            description: 'För att må bra behöver vi balans i tillvaron.',
            questions: [
              'Hur prioriterar du mellan konståkning och andra intressen? ',
              'Kan det vara svårt att välja?'
            ]
          },
          {
            id: 25,
            headline: 'Säsongsplanering',
            description: 'Det är dags att planera den kommande säsongen.',
            questions: [
              'Hur pratar ni om träningsupplägg och målsättning i din grupp/lag?'
            ]
          },
          {
            id: 26,
            headline: 'Sportsligt uppträdande',
            description: 'När du kliver av isen efter ett tävlingsåk känner du dig så nöjd. När poängen visas överensstämmer de inte med känslan.',
            questions: [
              'Hur hanterar du/ni det?',
              'Vad är viktigt att tänka på i Kiss & Cry?',
              'Hur viktiga är poängen för dig/er?'
            ]
          },
          {
            id: 27,
            headline: 'Träningsmiljö',
            description: 'Du går till tränaren och berättar om ett problem. Tränaren svarar oförstående på frågan och viftar bort ditt problem.',
            questions: [
              'Vad gör du?',
              'Om inte tränaren lyssnar, vem kan man prata med då?'
            ]
          },
          {
            id: 28,
            headline: 'Gemensamma mål',
            description: 'Du ingår i ett lag där alla varit delaktiga i att komma överens om ett gemensamt mål för säsongen. Några i laget är inte beredda att göra vad som krävs för att nå målet.',
            questions: [
              'Hur får man alla taggade?',
              'Borde man ställa krav/sänka krav?'
            ]
          },
          {
            id: 29,
            headline: 'Synkro',
            description: 'Din lagkamrat blir väldigt nervös precis innan tävling och upplevs nedstämd.',
            questions: [
              'Hur påverkas ditt humör?',
              'Vad kan du göra för att hjälpa din kamrat?'
            ]
          },
          {
            id: 30,
            headline: 'Motivation',
            description: 'Du/ditt lag har haft en tuff vecka och befinner er i en svacka.',
            questions: [
              'Hur gör ni för att ta er ur den?'
            ]
          },
          {
            id: 30,
            headline: 'Motivation',
            description: 'Du tränar på som vanligt men tappar motivationen.',
            questions: [
              'Hur kan din tränare motivera dig att fortsätta?',
              'Vad mer skulle kunna hjälpa dig att återfå motivationen?'
            ]
          }
        ]
      },
      {
        id: 2,
        name: 'Föräldrar',
        color: '#273976',
        headline: 'Diskussionskort föräldrar',
        info: [
          'Korten syftar till ökad förståelse för varandras roller i föreningen samt föräldrarnas ansvar och del i att skapa ett positivt klimat i ishallen.',
          'Dra ett kort vid föräldramötet för att på ett enkelt sätt få värdegrundsfrågan på agendan.'
        ],
        question_page: [
          {
            id: 1,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten. I ishallen träffar du en grupp föräldrar som diskuterar allt de tycker är fel med verksamheten.',
            questions: [
              'Hur agerar du?',
              'Hur bör man tänka för att sprida positiv energi?'
            ]
          },
          {
            id: 2,
            headline: 'Glädje, glöd och gemenskap',
            description: '',
            questions: [
              'Hur ofta berömmer du tränare, styrelsemedlemmar och andra eldsjälar i din förening som gör sitt bästa för att skapa en bra fritidsmiljö?'
            ]
          },
          {
            id: 3,
            headline: 'Förebilder',
            description: 'Inom svensk konståkning pratar vi om att vara förebilder och fostra förebilder',
            questions: [
              'Vad är en bra förebild?',
              'Hur kan du vara en bra förebild i din förening?'
            ]
          },
          {
            id: 4,
            headline: 'Tävlingsvanor',
            description: 'Ditt barn mår dåligt i samband med en tävling. Du har flera gånger uppmärksammat att barnet blir väldigt nervöst.',
            questions: [
              'Hur viktigt är det att ditt barn tävlar?',
              'Hur kan du avdramatisera tävlingsmomentet?',
              'Hur kan du stärka ditt barns självkänsla?'
            ]
          },
          {
            id: 5,
            headline: 'Hälsa',
            description: 'Ditt barn kommer hem från träning och berättar att de haft en diskussion kring vad som krävs för att lyckas. Du är orolig för att ditt barn efter denna diskussion kommer att fixera sig vid sin vikt',
            questions: [
              'Vad har du för erfarenheter kring ätstörningar?',
              'Hur säkerställer ni goda kostvanor hemma?',
              'Hur diskuterar du det här med ditt barn?'
            ]
          },
          {
            id: 6,
            headline: 'Träningsdos',
            description: 'Tränaren har höga ambitioner för ditt barn och ni har blivit erbjudna att träna mer. Ditt barn har inte viljan att öka träningsdosen.',
            questions: [
              'Hur hanterar du situationen?',
              'För vems skull idrottar ditt barn?'
            ]
          },
          {
            id: 7,
            headline: 'Motivation',
            description: 'Ditt barn har i fler år med glädje sysslat med olika idrotter, men kommer hem en dag och påstår att idrott är töntigt. Du anar att det kan bero på grupptrycket från kompisarna som för en tid sedan slutat med idrott.',
            questions: [
              'Hur hanterar du att ditt barn plötsligt vill sluta idrotta?'
            ]
          },
          {
            id: 8,
            headline: 'Misslyckande',
            description: 'Ditt barn deltar i en tävling men blir besviken eftersom resultatet inte överensstämmer med den upplevda prestationen. Trots att ditt barn presterar nära sitt max hamnar barnet långt ner i resultatlistan.',
            questions: [
              'Hur hjälper du ditt barn att hantera och förstå situationen?',
              'Hur förklarar man skillnaden mellan resultat och prestation?'
            ]
          },
          {
            id: 9,
            headline: 'Sportsligt uppträdande',
            description: 'Du och ditt barn är på en tävling och en okänd förälder kritiserar både åkarna och de tekniska funktionärerna.',
            questions: [
              'Hur agerar du?',
              'Hade du agerat annorlunda om du känt personen i fråga?',
              'Vad tycker du att din förening kan göra för att förebygga det beteendet?'
            ]
          },
          {
            id: 10,
            headline: 'Motivation',
            description: 'Ditt barn och hennes/hans bästa kompis åker konståkning i samma förening. Kompisen är en stor talang och tar ofta privatlektioner. Du märker med tiden att kompisen ofta ser trött ut och verkar ha tappat åkglädjen.',
            questions: [
              'Vad kan du göra?'
            ]
          },
          {
            id: 11,
            headline: 'Uppförande',
            description: 'Ditt barn åker konståkning i en förening. Nyligen började en ny tjej i gruppen och hennes föräldrar är ofta och tittar på träningarna. Föräldrarna utmärker sig genom att kommentera både åkarna på isen och tränarnas insatser.',
            questions: [
              'Vad trycker du om detta?',
              'Hur kan du agera i en liknande situation?'
            ]
          },
          {
            id: 12,
            headline: 'Skola vs idrott',
            description: 'Ditt barn lägger ner mycket tid på sin konståkningsträning och tycker det är jättekul. Däremot finns inte samma intresse för skolarbete och andra fritidsintressen.',
            questions: [
              'Hur ser du på denna situation?',
              'Hur tänker du på balansen mellan träning, skola och övrig tid?'
            ]
          },
          {
            id: 13,
            headline: 'Uppförande',
            description: 'En förälder uppmärksammar att en domare som tidigare underkänt hennes dotter på test ska döma tävlingen. Den där sopan borde bli av med sin domarlicens, utbrister mamman högt och tydligt.',
            questions: [
              'Hur reagerar du?',
              'Hur bör man agera som åskådare på läktaren?',
              'Hur gör vi för att även de tekniska funktionärerna ska få uppleva glädje, glöd och gemenskap inom konståkningen?'
            ]
          },
          {
            id: 14,
            headline: 'Roller och ansvar',
            description: 'En åkare behandlas på ett oschyst sätt av både tränaren och flera av klubbkamraterna. Åkarens föräldrar bestämmer sig för att kontakta styrelsen, men det känns jobbigt eftersom flera i styrelsen också är föräldrar till de som beter sig illa.',
            questions: [
              'Hur tänker ni kring detta?'
            ]
          },
          {
            id: 15,
            headline: 'Träningsvanor',
            description: 'Ditt barn tränar flera gånger i veckan och möjligheten till att ägna sig åt andra fritidssysselsättningar är minimal.',
            questions: [
              'Hur förhåller du dig till ditt barns träningsmotivation?',
              'Vad anser du är en sund mängd träning för ditt barn?'
            ]
          },
          {
            id: 16,
            headline: 'Mobbning',
            description: 'Ditt barn kommer hem från träningen och klagar på att träningskompisarna retas',
            questions: [
              'Hur hanterar du situationen?',
              'Vad säger du till ditt barn?',
              'Hur lär du ditt barn att vara en bra träningskompis?'
            ]
          },
          {
            id: 17,
            headline: 'Specialisering',
            description: 'Din dotter håller på med flera idrotter. En dag kommer hon hem och berättar att en tränare sagt åt henne att hon måste välja en idrott. Hennes svar, att hon inte vill välja, bemöttes inte med respekt.',
            questions: [
              'Hur hanterar du situationen?'
            ]
          },
          {
            id: 18,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten.',
            questions: [
              'Vad kan du göra för att bidra till ett sådant klimat?'
            ]
          }
        ]
      },
      {
        id: 3,
        name: 'Styrelse',
        color: '#5fb6d8',
        headline: 'Diskussionskort styrelse',
        info: [
          'Styrelsen har det yttersta ansvaret för verksamheten. Korten syftar till att förbereda styrelsen på situationer som kan uppstå och möta sina medlemmar på bästa sätt.',
          'Dra ett kort vid varje styrelsemöte för att på ett enkelt sätt få värdegrundsfrågan på agendan.'
        ],
        question_page: [
          {
            id: 1,
            headline: 'Ansvar',
            description: 'Föräldrar hör av sig till styrelsen och rapporterar om en rad incidenter som känns olikt er tränare. Ni misstänker att tränarens agerande beror på att tränaren inte mår bra.',
            questions: [
              'Hur agerar ni?',
              'Vilka skyldigheter har ni som arbetsgivare?'
            ]
          },
          {
            id: 2,
            headline: 'Delaktighet',
            description: '',
            questions: [
              'Genomför ni föräldramöten för att säkerställa att medlemmar få ta del av information?',
              'Välkomnar ni till en konstruktiv dialog?'
            ]
          },
          {
            id: 3,
            headline: 'Mobbning',
            description: 'Ni får signaler om att en av era tränare inte behandlas på ett schysst sätt av sina tränarkollegor. Tränaren inkluderas inte i teamet och får inte ta del av samma information som övriga tränare. Åkarna har noterat att de andra tränarna tittar snett på och hånar den utsatte tränaren.',
            questions: [
              'Hur agerar ni?',
              'Vilka skyldigheter har ni som arbetsgivare?',
              'Hur säkrar ni att ALLA föreningens tränare mår bra och behandlas på ett respektfullt sätt?'
            ]
          },
          {
            id: 4,
            headline: 'Resursfördelning',
            description: 'Det är en ständig kamp om istiderna i föreningen.',
            questions: [
              'Hur resonerar ni kring istidsfördelningen mellan grupper och olika typer av konståkningsverksamheter?',
              'Hur fördelar ni tränarresurserna?',
              'Hur ser diskussionsforumen ut?'
            ]
          },
          {
            id: 5,
            headline: 'Idrottsföreningen',
            description: 'I Sverige finns idag ca 20 000 idrottsföreningar, vilket gör idrottsrörelsen till Sveriges största folkrörelse.',
            questions: [
              'Varför tror ni att era medlemmar valt just er förening?',
              'Vad gör ni för att bli en ännu attraktivare förening? '
            ]
          },
          {
            id: 6,
            headline: 'Konståkningsföreningen',
            description: 'I Sverige finns idag ca 140 konståkningsföreningar.',
            questions: [
              'Vem finns er förening till för och har ni det i åtanke när ni planerar er träning eller fattar beslut kring verksamheten?',
              'På vilket sätt sätter ni åkarna i fokus?'
            ]
          },
          {
            id: 7,
            headline: 'Alkohol',
            description: 'En av era tränare kommer till träningen och ser trött och sliten ut. När du kommer nära tränaren känner du att personen luktar alkohol.',
            questions: [
              'Hur ställer ni er som styrelse kring detta?',
              'Känner du till om er förening har en alkohol- och drogpolicy?'
            ]
          },
          {
            id: 8,
            headline: 'Mångfald',
            description: 'En familj som precis har kommit till Sverige vill bli medlemmar i föreningen. Ni har svårt att kommunicera med familjen.',
            questions: [
              'Hur löser ni detta?',
              'Hur ser mångfalden ut i er förening?'
            ]
          },
          {
            id: 9,
            headline: 'Kulturskillnader',
            description: 'En tränare i er förening har en gedigen utländsk utbildning. Dock har tränaren begränsad kunskap om svensk idrott och dess värderingar.',
            questions: [
              'Hur uppmuntrar ni er tränare till vidareutbildning och erfarenhetsutbyte för att öka dennes förståelse och kunskap kring svensk idrott?',
              'Hur säkrar ni att tränarens gedigna kunskap landar på ett bra sätt i föreningen?'
            ]
          },
          {
            id: 10,
            headline: 'Rekrytering',
            description: 'Ni ska rekrytera en ny tränare till föreningen.',
            questions: [
              'Vilka egenskaper värderar ni högst?',
              'Vilken kompetens krävs för er verksamhet?',
              'Hur kommunicerar ni er inriktning och värdegrund för den nyrekryterade tränaren och hur ser introduktionsplanen ut?'
            ]
          },
          {
            id: 11,
            headline: 'Samarbete och delaktighet',
            description: '',
            questions: [
              'Hur skapar ni bra insyn i verksamheten för era medlemmar?',
              'Hur samarbetar styrelse och tränare för att tillsammans utveckla verksamheten och skapa en sund idrottsmiljö?',
              'Vilka mötesplatser finns för samtal och diskussion mellan olika roller inom föreningen?'
            ]
          },
          {
            id: 12,
            headline: 'Föreningens organisation',
            description: 'Flera i föreningen lägger ner stort ideellt engagemang.',
            questions: [
              'För att inte slita ut ett fåtal personer, hur skapar ni engagemang i föreningen?',
              'Hur bidrar detta till stärkt gemenskap och klubbkänsla?'
            ]
          },
          {
            id: 13,
            headline: 'Livslångt intresse för idrott',
            description: 'En åkare som åkt länge har tröttnat på att tävla och bestämt sig för att trappa ner. Hon kan tänka sig att bli teknisk funktionär och på så sätt stanna i sporten.',
            questions: [
              'Vilka träningsalternativ erbjuder ni i er förening?',
              'Hur ser ni på olika karriärvägar för era åkare?'
            ]
          },
          {
            id: 14,
            headline: 'Sociala medier',
            description: 'Händer det att du ser olämpliga kommentarer i sociala medier som rör konståkning?',
            questions: [
              'Har du något exempel?',
              'Hur tacklar vi den typen av beteende?'
            ]
          },
          {
            id: 15,
            headline: 'Sociala medier',
            description: 'Sociala medier har idag stort inflytande i vårt samhälle. Många föreningar ser ett stort värde i att synas i sociala medier och nå ut med viktig information.',
            questions: [
              'Hur använder sig er förening av sociala medier?',
              'Vilka för- och nackdelar ser vi?'
            ]
          },
          {
            id: 16,
            headline: 'Mobbning',
            description: 'Det uppdagas att en åkare blivit mobbad av en annan åkare. Tränaren får reda på detta samtidigt som ni i styrelsen.',
            questions: [
              'Hur kan ni i styrelsen stötta åkaren och tränaren?',
              'Har föreningen någon handlingsplan?',
              'Vad gör ni för att förebygga mobbning?'
            ]
          },
          {
            id: 17,
            headline: 'Trygga idrottsmiljöer',
            description: 'Ni har länge varit utan tränare när ni rekryterar en ny tränare med gott rykte. Efter en tid får ni signaler att åkare känner sig otrygga.',
            questions: [
              'Hur säkerställer ni en trygg idrottsmiljö?',
              'Vad har er förening för rutiner när ni ska rekrytera tränare?'
            ]
          },
          {
            id: 18,
            headline: 'Träningsupplägg',
            description: 'En förälder tycker att de aktiva bör träna hårdare. Tränaren har gått föräldern till mötes och stegrat träningen mer än vad ni anser lämpligt. Detta har lett till att andra föräldrar funderar på att flytta sina barn till en annan förening.',
            questions: [
              'Hur agerar ni i styrelsen?',
              'Hur kan ni möta föräldrar som har åsikter om föreningens verksamhet?',
              'Hur stöttar ni era tränare i deras uppdrag?'
            ]
          },
          {
            id: 19,
            headline: 'Träningsgrupper',
            description: 'En åkare har under senaste tiden börjat tappa intresset. Hon vill flytta till en annan träningsgrupp där hon har fler kompisar. Åkarens föräldrar har pratat med tränaren utan resultat och har nu ställt frågan till er.',
            questions: [
              'Hur agerar ni?',
              'Hur sker gruppindelningen idag?',
              'Hur får ni era åkare att utvecklas efter sina egna förutsättningar och ambitioner?'
            ]
          },
          {
            id: 20,
            headline: 'Verksamhetsinriktning',
            description: 'I er förenings verksamhetsidé står att ni ska bedriva en verksamhet där alla får vara med på sina egna villkor. En tränare tycks ha en annan uppfattning.',
            questions: [
              'Hur tydliga är ni i er förening med vilken typ av verksamhet ni vill bedriva?',
              'Hur löser ni situationer där tränarna har olika uppfattning?',
              'Behöver er förening se över verksamhetsidén?'
            ]
          },
          {
            id: 21,
            headline: 'Allas lika värde',
            description: 'En tränare berömmer och lyfter alltid fram samma åkare i gruppen.',
            questions: [
              'Hur för ni en bra dialog med tränaren kring detta?',
              'Är lika alltid rättvist?'
            ]
          },
          {
            id: 22,
            headline: 'Hälsa',
            description: 'Du märker att flera åkare i er förening har blivit smala och visar tecken på att de inte mår bra.',
            questions: [
              'Hur hanterar styrelsen situationen?'
            ]
          },
          {
            id: 23,
            headline: 'Motivation',
            description: 'En åkare pressas hårt av sina föräldrar. Åkaren tränar redan flera extrapass. På sistone har åkaren börjat få svårt att prestera och verkar inte tycka att det är roligt längre.',
            questions: [
              'Hur agerar ni?',
              'Hur tycker ni att föräldrar ska förhålla sig till sina barns idrottande?'
            ]
          },
          {
            id: 24,
            headline: 'Livslångt intresse för idrott',
            description: 'En teknisk funktionär som tillhör er förening känner stor glädje i domaryrket och hoppas på fortsatt utveckling i sin domarkarriär. Hon känner sig tyvärr åsidosatt och bortglömd i föreningens verksamhet.',
            questions: [
              'Hur säkrar ni att alla känner sig inkluderade i verksamheten?',
              'Har ni utvecklingssamtal med föreningens tekniska funktionärer?'
            ]
          },
          {
            id: 25,
            headline: 'Språkbruk',
            description: 'I ishallen hör du ofta fula ord i samband med att någon misslyckas eller blir irriterad.',
            questions: [
              'Vilka regler finns i er förening?',
              'Vilket språkbruk tycker du är okej under träning och tävling?',
              'Hur hjälps vi åt att skapa ett positivt klimat i ishallen?'
            ]
          },
          {
            id: 26,
            headline: 'Uppförande',
            description: 'En förälder uppmärksammar att en domare som tidigare underkänt hennes dotter på test ska döma tävlingen. Den där sopan borde bli av med sin domarlicens, utbrister mamman högt och tydligt',
            questions: [
              'Hur agerar du?',
              'Hur utbildar ni föreningens föräldrar?'
            ]
          },
          {
            id: 27,
            headline: 'Ledarskap',
            description: 'Föreningens väletablerade tränare har en hård ton mot sina mer oerfarna kollegor',
            questions: [
              'Vilket typ av ledarskap förespråkar ni i er förening?',
              'Hur säkrar du ett positivt arbetsklimat i tränarkåren?'
            ]
          },
          {
            id: 28,
            headline: 'Säkerhet',
            description: 'En åkare ramlar och slår upp hakan.',
            questions: [
              'Vad har ni för rutiner vid olyckor?',
              'Har ni en första-bands-låda?',
              'Vem ansvarar för att den är komplett?'
            ]
          },
          {
            id: 29,
            headline: 'Hälsa',
            description: 'Er tränare känner sig otillräcklig och stressad.',
            questions: [
              'På vilket sätt kan ni stötta och underlätta för tränaren?'
            ]
          },
          {
            id: 30,
            headline: 'Roller och ansvar',
            description: 'En åkare behandlas på ett oschyst sätt av både tränaren och flera av klubbkamraterna. Åkarens föräldrar bestämmer sig för att kontakta styrelsen, men det känns jobbigt eftersom flera i styrelsen också är föräldrar till de som beter sig illa.',
            questions: [
              'Hur tänker ni kring detta?',
              'Hur håller man isär föräldrarollen och styrelserollen?'
            ]
          },
          {
            id: 31,
            headline: 'Sociala medier',
            description: 'Åkarna tar bilder både i och utanför omklädningsrummet. Ni är oroliga för hur bilderna sprids.',
            questions: [
              'Hur agerar ni?',
              'Vad har ni för regler?'
            ]
          },
          {
            id: 32,
            headline: 'Uppförande',
            description: 'En tränare kontaktar föreningens styrelse på grund av att en åkare i gruppen uppträder illa säger provocerande saker till honom.',
            questions: [
              'Hur agerar ni?',
              'Hur stöttar ni er tränare?'
            ]
          },
          {
            id: 33,
            headline: 'Resursfördelning',
            description: 'Föräldrar till några av klubbens tävlingsåkare är missnöjda över att deras barn får dela isbanan med skridskoskolan. Det är ett utmärkt tillfälle för tävlingsgruppen att öva piruetter, men föräldrarna är ändå missnöjda med upplägget.',
            questions: [
              'Hur hanterar ni detta?'
            ]
          },
          {
            id: 34,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten.',
            questions: [
              'Vad kan du göra för att bidra till ett sådant klimat?'
            ]
          },
          {
            id: 35,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten. På isen förekommer hårda ord och flera åkare är ofta ledsna efter träningen.',
            questions: [
              'Hur säkrar ni som styrelse att värdegrunden blir verklighet?',
              'Hur jobbar era tränare för att sätta åkglädjen i fokus?'
            ]
          },
          {
            id: 36,
            headline: 'Osynlig funktionsnedsättning',
            description: 'Till er förening kommer en åkare som har en osynlig funktionsnedsättning. Tränaren märker att åkaren inte har samma utvecklingskurva som de andra aktiva och vänder sig till styrelsen för råd.',
            questions: [
              'Hur arbetar ni för att inkludera personer med funktionsnedsättningar i er verksamhet?',
              'Hur kan ni lära er mer om olika typer av funktionsnedsättningar?'
            ]
          },
          {
            id: 37,
            headline: 'Inkluderande idrott',
            description: '',
            questions: [
              'Hur arbetar ni i er förening för att fler ska vilja vara med?',
              'Hur håller vi kostnaderna nere så fler kan vara med?'
            ]
          },
          {
            id: 38,
            headline: 'Åsikter och värderingar',
            description: 'Några av föreningens föräldrar verkar inte införstådda i föreningens värderingar.',
            questions: [
              'Hur arbetar föreningen med att kommunicera sina värderingar?',
              'Vilka informations- och diskussionsforum finns i er förening?'
            ]
          },
          {
            id: 39,
            headline: 'Delaktighet',
            description: 'Ökad delaktighet och en ökad känsla av självbestämmande leder till åkare med en egen inre drivkraft.',
            questions: [
              'Hur säkerställer ni att era medlemmar känner sig delaktiga?',
              'Genomförs åkarsamtal i föreningen?'
            ]
          }
        ]
      },
      {
        id: 4,
        name: 'Tekniska funktionärer',
        color: '#579c47',
        headline: 'Diskussionskort tekniska funktionärer',
        info: [
          'Korten syftar till ökad förståelse för den tekniska funktionärens roll i föreningen samt förbättrat samarbete inom domarkåren.',
          'Dra ett kort vid domarmötet, utbildningen eller i möten med din förening för att på ett enkelt sätt få värdegrundsfrågan på agendan.'
        ],
        question_page: [
          {
            id: 1,
            headline: 'Hälsa',
            description: 'Händer det att du känner dig påhoppad av tränare eller domarkollegor som ifrågasätter din bedömning.',
            questions: [
              'Hur jobbar du för att stärka ditt självförtroende i din roll?',
              'Vad får du för stöd från din förening och dina kollegor?'
            ]
          },
          {
            id: 2,
            headline: 'Förebild',
            description: 'Inom svensk konståkning pratar vi om att vara förebilder och fostra förebilder.',
            questions: [
              'Vad är en bra förebild?',
              'Hur kan du inspirera åkare till att välja rollen som teknisk funktionär?'
            ]
          },
          {
            id: 3,
            headline: 'Gemenskap',
            description: '',
            questions: [
              'Vilka samarbetsmöjligheter finns mellan olika roller?',
              'Hur skapar du samarbeten med kollegor och klubbkamrater?'
            ]
          },
          {
            id: 4,
            headline: 'Roller',
            description: 'Många vittnar om att det handlar om att få frågan om att gå en utbildning.',
            questions: [
              'Vad fick dig att ta klivet in i domar-karriären?',
              'Vad är roligast med att vara teknisk funktionär?'
            ]
          },
          {
            id: 5,
            headline: 'Uppförande',
            description: 'Du dömer en tävling och känner att nivån på åken ligger under förväntan.',
            questions: [
              'Hur pratar vi om åkarna och åken på ett respektfullt sätt?'
            ]
          },
          {
            id: 6,
            headline: 'Sociala medier',
            description: 'En förälder filmar en test från läktaren och publicerar materialet i sociala medier. På Facebook går kommentarerna varma. En tränare som ser materialet ringer upp domaren och ifrågasätter bedömningen.',
            questions: [
              'Är det okej att publicera den här typen av material?',
              'Är det okej att ifrågasätta domarens jobb utifrån det?'
            ]
          },
          {
            id: 7,
            headline: 'Field of Play Decisions',
            description: 'Du blir uppringd av en tränare efter avslutad tävling. Tränaren är ursinnig för att distriktets åkare fått låga poäng och därför inte säkrat en plats i Elitserien.',
            questions: [
              'Hur agerar du?',
              'Vilket stöd behöver du av din förening?'
            ]
          },
          {
            id: 8,
            headline: 'Respekt',
            description: 'Ni har precis avslutat en tävling. En tränare avbryter domarmötet för att påtala något som han anser vara felaktigt bedömt.',
            questions: [
              'Hur skapar man förståelse mellan tränare och tekniska funktionärer?'
            ]
          },
          {
            id: 9,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten. På ett test tycker domarna att nivån varit på tok för låg.',
            questions: [
              'Hur kommunicerar man detta till åkarna på ett sätt som peppar dem att jobba vidare?',
              'Hur bör man tänka för att sprida positiv energi?'
            ]
          },
          {
            id: 10,
            headline: 'Sociala medier',
            description: 'Händer det att du ser olämpliga kommentarer i sociala medier som rör konståkning?',
            questions: [
              'Har du något exempel?',
              'Hur tacklar vi den typen av beteende?'
            ]
          },
          {
            id: 11,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten.',
            questions: [
              'Vad kan du göra för att bidra till ett sådant klimat?'
            ]
          },
          {
            id: 12,
            headline: 'Roller & ansvar',
            description: 'Det bästa sättet att öka förståelsen för varandras roller är att skapa tillfällen för möten och samtal.',
            questions: [
              'Hur jobbar er förening för att öka förståelsen för domaryrket bland åkare, föräldrar, tränare och styrelsen?',
              'Har du haft övergripande informationsträffar för åkare, föräldrar, styrelse eller tränare kring t.ex. tester eller tävlingar?'
            ]
          },
          {
            id: 13,
            headline: 'Roller & ansvar',
            description: 'Du dömer en tävling och på väg till domarrummet i en paus stöter du på en gammal konståkningsbekant. Du blir glad att se personen, men du får ju egentligen inte prata någon annan än skiljedomaren.',
            questions: [
              'Hur hanterar du situationen utan att verka snobbig eller ignorant?'
            ]
          },
          {
            id: 14,
            headline: 'Livslångt intresse för idrott',
            description: 'Vid ett ”round table-möte” är skiljedomaren starkt kritisk mot såväl nivån på domarna som åkarna på tävlingen. Flera unga domare känner sig starkt ifrågasatta men också hånade och uthängda inför sina domarkollegor.',
            questions: [
              'Hur kommunicerar du med dina kollegor?',
              'Behandlas alla i panelen likvärdigt?',
              'Hur framför vi kritik på ett konstruktivt sätt som inspirerar till vidareutbildning och fortsatt engagemang hos hela vår domarkår?'
            ]
          },
          {
            id: 15,
            headline: 'Gränser',
            description: 'Du är i ishallen i ett privat ärende och stöter på en förälder som passar på att ställa frågor om bedömning vid tävling och test.',
            questions: [
              'Hur bemöter du föräldern?',
              'Hur förklarar du att du kanske inte kan hantera frågor om du har bråttom för att passa en tid?'
            ]
          },
          {
            id: 16,
            headline: 'Delaktighet',
            description: 'Ökad delaktighet och en ökad känsla av självbestämmande leder till individer med en egen inre drivkraft och påverkar således vårt intresse att fortsätta.',
            questions: [
              'Hur blir du inkluderad i din förening?',
              'Erbjuds du utvecklingssamtal av din förening?'
            ]
          }
        ]
      },
      {
        id: 5,
        name: 'Tränare',
        color: '#f7c737',
        headline: 'Diskussionskort tränare',
        info: [
          'Korten syftar till ökad förståelse för varandras roller i föreningen och förbundet samt förbättrat samarbete inom tränarteamet.',
          'Dra ett kort vid varje tränarmöte för att på ett enkelt sätt få värdegrundsfrågan på agendan.'
        ],
        question_page: [
          {
            id: 1,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten.',
            questions: [
              'Vad kan du göra för att bidra till ett sådant klimat?'
            ]
          },
          {
            id: 2,
            headline: 'Glädje, glöd och gemenskap',
            description: 'Du och din förening är en del av svensk konståkning. Vi har en tydlig värdegrund där glädje, glöd och gemenskap ska genomsyra verksamheten.',
            questions: [
              'Hur påverkar detta ditt ledarskap och ditt agerande på isen?',
              'Hur jobbar ni i tränarteamet för att sätta åkglädjen i fokus?'
            ]
          },
          {
            id: 3,
            headline: 'Träningsgrupper',
            description: 'En ny åkare börjar i er förening. Socialt bidrar åkaren till en positiv stämning som lyfter gruppen/laget. Men tekniskt håller inte åkaren samma nivå som de andra.',
            questions: [
              'Hur gör du för att bedriva en träning som passar alla och samtidigt motiverar dem?',
              'Hur värderar ni de sociala aspekterna i er förening?'
            ]
          },
          {
            id: 4,
            headline: 'Osynlig funktionsnedsättning',
            description: 'Du har en åkare i din grupp som har en osynlig funktionsnedsättning. Du märker att åkaren inte har samma utvecklingskurva som de andra i gruppen och träningskompisarna verkar ta avstånd.',
            questions: [
              'Hur hanterar du situationen?',
              'Hur arbetar ni för att respektera varandras olikheter?',
              'Hur kan ni lära er mer om olika typer av funktionsnedsättningar?'
            ]
          },
          {
            id: 5,
            headline: 'Tränarrollen',
            description: 'Inom den svenska idrottsrörelsen är vi ca 600 000 ledare och tillsammans är vi nödvändiga för att svensk idrott ska fortsätta vara stark och utvecklas till världens bästa.',
            questions: [
              'Varför har du valt att bli tränare?',
              'Vad driver dig i din tränarroll?',
              'Vad motiverar dig till att fortsätta som tränare inom konståkningen?'
            ]
          },
          {
            id: 6,
            headline: 'Föräldrar',
            description: 'Föräldrarna till en av åkarna i din grupp är väldigt engagerade i såväl träning som tävling. Nu har de börjat ifrågasätta ditt träningsupplägg.',
            questions: [
              'Hur agerar du?',
              'Hur arbetar du för att främja dialogen med föräldrarna?',
              'Vilket stöd behöver du från din förening?'
            ]
          },
          {
            id: 7,
            headline: 'Tränarfilosofi',
            description: 'Många framgångsrika ledare pratar om att de har en grundläggande ledarfilosofi som de jobbar efter.',
            questions: [
              'Vad är grundläggande för dig i ditt ledarskap?',
              'Hur arbetar du för att aktivt utveckla ditt ledarskap?'
            ]
          },
          {
            id: 8,
            headline: 'Kosttillskott',
            description: 'Det har visat sig att flera idrottare som åkt fast i dopingkontroller använt sig av kosttillskott som innehåller otillåtna ämnen utan att de vetat om det.',
            questions: [
              'Vad är din inställning till kosttillskott?',
              'Hur ger du dina åkare vägledning kring ämnet?'
            ]
          },
          {
            id: 9,
            headline: 'Tränarkonflikter',
            description: 'Du och din tränarkollega har många gånger skilda åsikter om hur träningen ska bedrivas och vem som ska vara i vilken träningsgrupp.',
            questions: [
              'Hur hanterar ni situationen så att det inte går ut över åkarna?',
              'Kring vilka frågor kan det uppstå konflikter mellan tränarna i din förening?',
              'Vad är viktigt för att ett samarbete ska fungera?'
            ]
          },
          {
            id: 10,
            headline: 'Hälsa',
            description: 'En av dina kollegor kommer till träningen och ser trött och sliten ut.',
            questions: [
              'Hur hjälps ni åt i tränarteamet?',
              'Vad gör ni för att alla ska må bra?'
            ]
          },
          {
            id: 11,
            headline: 'Motivation',
            description: 'En åkare i en av dina grupper har satsat helhjärtat på singelåkning. Nu har åkaren bestämt sig för att välja isdans istället.',
            questions: [
              'Hur reagerar och agerar du?',
              'Hur kan du möjliggöra för en åkare att syssla med två discipliner samtidigt?'
            ]
          },
          {
            id: 12,
            headline: 'Säkerhet',
            description: 'En åkare ramlar och slår upp hakan.',
            questions: [
              'Vad har ni för rutiner vid olyckor?',
              'Har ni en första-bands-låda?',
              'Vem ansvarar för att den är komplett?'
            ]
          },
          {
            id: 13,
            headline: 'Resursfördelning',
            description: 'Det är en ständig kamp om istiderna i föreningen.',
            questions: [
              'Hur resonerar ni kring istidsfördelningen mellan grupper och olika typer av konståkningsverksamhet?',
              'Hur fördelar ni tränarresurserna?',
              'Hur ser diskussionsforumen ut?'
            ]
          },
          {
            id: 14,
            headline: 'Idrottsföreningen',
            description: 'I Sverige finns idag ca 20 000 idrottsföreningar vilket gör idrottsrörelsen till Sveriges största folkrörelse.',
            questions: [
              'Varför tror ni att era medlemmar valt just er förening?',
              'Vad gör ni för att bli en ännu attraktivare förening?'
            ]
          },
          {
            id: 15,
            headline: 'Motivation',
            description: 'Du har tränat en åkare i flera år och ni kommer bra överens. Du ser hennes potential och trivs med ditt tränaruppdrag. En dag berättar hon att hon fortfarande vill träna men inte satsa hårt på tävlandet.',
            questions: [
              'Hur reagerar och agerar du?',
              'Hur ser du på aktiva som vill träna men inte tävla?',
              'Hur kan man skapa en bra verksamhet för alla oavsett ambitionsnivå?'
            ]
          },
          {
            id: 16,
            headline: 'Konståkningsföreningen',
            description: 'I Sverige finns idag ca 140 konståkningsföreningar.',
            questions: [
              'Vem finns er förening till för?',
              'Har ni det i åtanke när ni planerar er träning eller fattar beslut kring verksamheten?',
              'På viket sätt sätter ni åkarna i fokus?'
            ]
          },
          {
            id: 17,
            headline: 'Mångfald',
            description: 'En familj som precis har kommit till Sverige vill bli medlemmar i föreningen. Du har svårt att kommunicera med familjen.',
            questions: [
              'Hur löser du detta?',
              'Hur kan föreningen hjälpa familjen att få kunskap om den svenska idrottsrörelsen och föreningslivet?'
            ]
          },
          {
            id: 18,
            headline: 'Åsikter och värderingar',
            description: 'Några av föreningens föräldrar verkar inte införstådda i föreningens värderingar.',
            questions: [
              'Hur arbetar föreningen med att kommunicera sina värderingar?',
              'Vilka informations- och diskussionsforum finns i er förening?'
            ]
          },
          {
            id: 19,
            headline: 'Samarbete och delaktighet',
            description: '',
            questions: [
              'Hur skapar ni bra insyn/samsyn i verksamheten för era medlemmar?',
              'Hur samarbetar styrelse och tränare för att tillsammans utveckla verksamheten och skapa en sund idrottsmiljö?',
              'Vilka mötesplatser finns för samtal och diskussion mellan olika roller inom föreningen?'
            ]
          },
          {
            id: 20,
            headline: 'Delaktighet',
            description: 'Ökad delaktighet och en ökad känsla av självbestämmande leder till åkare med en egen inre drivkraft.',
            questions: [
              'Hur säkerställer ni att åkarna känner sig delaktiga?',
              'Genomför ni åkarsamtal för att komma överens om mål och träningsupplägg?'
            ]
          },
          {
            id: 21,
            headline: 'Förebild',
            description: 'Inom svensk konståkning pratar vi om att vara förebilder och fostra förebilder.',
            questions: [
              'Vad är en god förebild?',
              'Vem är din förebild och varför?'
            ]
          },
          {
            id: 22,
            headline: 'Föreningens organisation',
            description: '',
            questions: [
              'Hur ser ditt uppdrag ut?',
              'Vilka är dina rättigheter och skyldigheter?'
            ]
          },
          {
            id: 23,
            headline: 'Träningsgrupper',
            description: 'En åkare har under senaste tiden börjat tappa intresset. Hon vill flytta till en annan träningsgrupp/lag där hon har fler kompisar.',
            questions: [
              'Hur agerar du?',
              'Hur sker indelningen idag?',
              'Hur får ni era åkare att utvecklas efter sina egna förutsättningar och ambitioner?'
            ]
          },
          {
            id: 24,
            headline: 'Verksamhetsinriktning',
            description: 'I er förenings verksamhetsidé står att ni ska bedriva en verksamhet där alla får vara med på sina egna villkor. Ni har också riktlinjer kring eventuell elitsatsning. Trots detta har du och dina kollegor olika uppfattning om hur verksamheten bör bedrivas.',
            questions: [
              'Hur tydliga är ni i er förening med vilken typ av verksamhet ni vill bedriva?',
              'Hur löser ni situationer där ni tränare har olika uppfattning?',
              'Behöver er förening se över verksamhetsidén?'
            ]
          },
          {
            id: 25,
            headline: 'Sociala medier',
            description: 'Åkarna tar bilder både i och utanför omklädningsrummet. Du är orolig för hur bilderna sprids.',
            questions: [
              'Hur agerar du?',
              'Vad har ni för regler?'
            ]
          },
          {
            id: 26,
            headline: 'Hälsa',
            description: 'Du vaknar på morgonen och känner dig febrig. Ikväll är det återigen dags för träning.',
            questions: [
              'Var går gränsen för när ni avstår träning och tävling på grund av sjukdom?'
            ]
          },
          {
            id: 27,
            headline: 'Självkänsla',
            description: 'Du har en åkare i gruppen som ofta hittar fel på sig själv och sina prestationer. Du ger beröm, men åkaren svarar alltid med att påtala allt hon gjorde fel.',
            questions: [
              'Hur jobbar ni med detta?'
            ]
          },
          {
            id: 28,
            headline: 'Mobbning',
            description: 'En åkare behandlas på ett oschyst sätt både av din tränarkollega och flera av klubbkamraterna.',
            questions: [
              'Hur agerar du?',
              'Vad behöver du för stöd från föreningen?'
            ]
          },
          {
            id: 29,
            headline: 'Motivation',
            description: 'En åkare i din grupp pressas hårt av sina föräldrar. Åkaren tränar redan flera extrapass. På sistone har åkaren börjat få svårt att prestera och verkar inte tycka att det är roligt längre.',
            questions: [
              'Hur agerar du?',
              'Hur tycker du att föräldrar ska förhålla sig till sina barns idrottande?',
              'Hur kommunicerar du dina reflektioner med åkarens föräldrar?'
            ]
          },
          {
            id: 30,
            headline: 'Uppförande',
            description: 'En förälder uppmärksammar att en domare som tidigare underkänt hennes dotter på test ska döma tävlingen. Den där sopan borde bli av med sin domarlicens, utbrister mamman högt och tydligt.',
            questions: [
              'Hur agerar du?',
              'Hur utbildar ni föreningens föräldrar?',
              'Hur säkrar vi att även domarna upplever glädje, glöd och gemenskap inom konståkningen?'
            ]
          },
          {
            id: 31,
            headline: 'Ledarskap',
            description: 'Föreningens väletablerade tränare har en hård ton mot sina mer oerfarna kollegor.',
            questions: [
              'Vilket typ av ledarskap förespråkar ni i er förening?',
              'Hur säkrar du ett positivt arbetsklimat i tränarkåren?'
            ]
          },
          {
            id: 32,
            headline: 'Träning vs skola',
            description: 'En åkare i din grupp lägger ner mycket tid på sin konståkningsträning och tycker det är jättekul. Däremot finns inte samma intresse för skolarbete och andra fritidsintressen.',
            questions: [
              'Hur ser du som tränare på denna situation?',
              'Hur tänker du på balansen mellan träning, skola och övrig tid?'
            ]
          },
          {
            id: 33,
            headline: 'Livslångt idrottande',
            description: 'En åkare som åkt länge har tröttnat på att tävla och skulle vilja trappa ner på träningen. På sikt kan hon tänka sig att utbilda sig till teknisk funktionär',
            questions: [
              'Hur reagerar du?',
              'Vilka träningsalternativ erbjuder ni i er förening?',
              'Hur ser du på olika karriärvägar för åkarna?',
              'Hur kan du stötta och vägleda?'
            ]
          },
          {
            id: 34,
            headline: 'Acceptera spelets regler',
            description: 'Du ringer upp en domare efter avslutad tävling eftersom du är besviken över att inte distriktets åkare fått bättre poäng i tävlingen för att kunna säkra en plats i Elitserien nästa säsong.',
            questions: [
              'Hur förhåller vi oss till bedömning av dagens prestation?'
            ]
          },
          {
            id: 35,
            headline: 'Resursfördelning',
            description: 'Föräldrar till några av klubbens tävlingsåkare är missnöjda över att deras barn får dela isbanan med skridskoskolan. Det är ett utmärkt tillfälle för en av tävlingsgrupperna att öva piruetter, men föräldrarna är ändå missnöjda med upplägget.',
            questions: [
              'Hur hanterar du detta?',
              'Hur talar ni om allas lika värde i er förening?'
            ]
          },
          {
            id: 36,
            headline: 'Sociala medier',
            description: 'En förälder filmar en test från läktaren och publicerar materialet i sociala medier. På Facebook går kommentarerna varma. En tränare som ser materialet ringer upp domaren och ifrågasätter bedömningen.',
            questions: [
              'Är det okej att publicera den här typen av material?',
              'Är det okej att ifrågasätta domarens jobb utifrån det?',
              'Hur tror du att domaren känner sig?',
              'Hur tror du att den filmade åkaren känner sig?'
            ]
          },
          {
            id: 37,
            headline: 'Trygghet',
            description: 'I din grupp skiljer sig nivån mellan åkarna mycket och vissa är bättre än andra. Du får höra att när du inte märker ger några av åkarna nedsättande kommentarer och blickar åt de andra.',
            questions: [
              'Hur agerar du?',
              'Hur har ni kommit överens om att man ska vara mot varandra i gruppen?',
              'Hur får vi alla att känna sig välkomna?'
            ]
          },
          {
            id: 38,
            headline: 'Gemenskap',
            description: 'Du som tränare märker att en åkare hamnar utanför i gruppen/laget och oftast är för sig själv.',
            questions: [
              'Vad kan du göra för att inkludera alla i gruppen/laget?'
            ]
          },
          {
            id: 39,
            headline: 'Träningsdos',
            description: 'Vad anser du är en rimlig träningsdos utifrån din grupp och dess mål?',
            questions: [
              'Hur för du en dialog kring detta med åkarna och deras föräldrar?'
            ]
          },
          {
            id: 40,
            headline: 'Sociala medier',
            description: 'En åkare i din grupp skriver ofta nedvärderande saker om andra åkare i sociala medier.',
            questions: [
              'Hur agerar du?'
            ]
          },
          {
            id: 41,
            headline: 'Hälsa',
            description: '',
            questions: [
              'Hur förhåller du dig till kost och vikt med de åkare du tränar?',
              'Hur undviker ni hets kring kroppsfixering?'
            ]
          },
          {
            id: 42,
            headline: 'Pubertet',
            description: 'En av åkarna har nått puberteten tidigt, först av alla. De andra tisslar och tasslar bakom ryggen på åkaren.',
            questions: [
              'Hur hanterar du situationen?',
              'Hur förklarar du svackor i samband med puberteten för dina åkare och deras föräldrar?'
            ]
          },
          {
            id: 43,
            headline: 'Teamkänsla',
            description: '',
            questions: [
              'Hur är man en bra tränarkollega?',
              'Hur bygger man ett framgångsrikt tränarteam?'
            ]
          },
          {
            id: 44,
            headline: 'Uttagning',
            description: '',
            questions: [
              'Hur sker uttagning inför tävling i ditt lag/din förening?',
              'Har laget/föreningen någon policy för detta?',
              'Hur förklarar du uttagningsreglerna för dina åkare och deras föräldrar?'
            ]
          },
          {
            id: 45,
            headline: 'Träningsvanor',
            description: 'Din grupp tränar flera gånger i veckan vilket minimerar möjligheten för åkarna att utöva andra fritidssysselsättningar.',
            questions: [
              'Hur förhåller du dig till åkarnas träningsmotivation?',
              'Vad anser du är en sund mängd träning för åkarna?',
              'Hur kan du som tränare säkra att åkarna får balans i tillvaron?'
            ]
          },
          {
            id: 46,
            headline: 'Tävlingsvanor',
            description: 'En av åkarna i gruppen du tränar mår dåligt i samband med en tävling. Du har flera gånger uppmärksammat att åkaren är nervös och orolig.',
            questions: [
              'Hur viktigt är det att åkarna i din grupp tävlar?',
              'Hur kan du hjälpa åkaren att slippa må så dåligt?',
              'Hur förbereder du åkarna och eventuellt deras föräldrar inför tävling?'
            ]
          },
          {
            id: 47,
            headline: 'Laguttagning',
            description: 'Laget ska tas ut till kommande tävling och du ska berätta för en åkare att den ska vara reserv.',
            questions: [
              'Hur går du till väga för att den åkaren inte ska känna sig åsidosatt?',
              'Hur får du åkaren att känna sig motiverad att kämpa vidare?'
            ]
          },
          {
            id: 48,
            headline: 'Misslyckande',
            description: 'En åkare deltar i en tävling men blir besviken eftersom resultatet inte överensstämmer med den upplevda prestationen. Trots att åkaren presterar nära sitt max hamnar åkaren långt ner i resultatlistan.',
            questions: [
              'Hur hjälper du åkaren att hantera och förstå situationen?',
              'Hur förklarar man skillnaden mellan resultat och prestation?',
              'Hur kan ni sätta upp inspirerande mål för åkaren?'
            ]
          },
          {
            id: 49,
            headline: 'Föräldrar',
            description: '',
            questions: [
              'Hur resonerar ni kring att föräldrar sitter på läktaren och tittar under träning?'
            ]
          },
          {
            id: 50,
            headline: 'Förebilder',
            description: 'Inom svensk konståkning pratar vi om att vara förebilder och fostra förebilder.',
            questions: [
              'Hur kan du vara en bra förebild för dina kollegor?',
              'Hur kan du vara en bra förebild för åkarna?'
            ]
          },
          {
            id: 51,
            headline: 'Motivation',
            description: '',
            questions: [
              'Hur gör du för att alla i din grupp/ditt lag ska känna sig sedda?',
              'Hur får du alla att känna sig inkluderade?'
            ]
          },
          {
            id: 52,
            headline: 'Sportsligt uppträdande',
            description: '',
            questions: [
              'Hur för man diskussioner om judges details på ett sportsmannamässigt sätt?'
            ]
          },
          {
            id: 53,
            headline: 'Inkluderande idrott',
            description: '',
            questions: [
              'Hur arbetar ni i er förening för att fler ska vilja vara med?',
              'Hur håller vi kostnaderna nere så fler kan vara med?'
            ]
          }
        ]
      },
      {
        id: 6,
        name: 'Trygg i ishallen',
        color: '#6d6b96',
        headline: 'Diskussionskort Trygg i ishallen',
        info: [
          'Dessa kort passar såväl barn som övriga målgrupper.',
          'Syftet med korten är att främja kamratskap och trivsel samt öka förståelsen för hur vi ska vara mot varandra. Ishallen ska vara en positiv och trygg plats.',
          'Dra ett kort och avsluta den påbörjade meningen.'
        ],
        question_page: [
          {
            id: 1,
            headline: '',
            description: 'Jag trivs som bäst i ishallen när…',
            questions: [
              ''
            ]
          },
          {
            id: 2,
            headline: '',
            description: 'Jag mår som bäst när…',
            questions: [
              ''
            ]
          },
          {
            id: 3,
            headline: '',
            description: 'När det kommer en ny åkare till klubben är det viktigt att…',
            questions: [
              ''
            ]
          },
          {
            id: 4,
            headline: '',
            description: 'Jag gillar när andra är…',
            questions: [
              ''
            ]
          },
          {
            id: 5,
            headline: '',
            description: 'När man tillhör en grupp är det viktigt att…',
            questions: [
              ''
            ]
          },
          {
            id: 6,
            headline: '',
            description: 'När man är ny i en grupp är det skönt om…',
            questions: [
              ''
            ]
          },
          {
            id: 7,
            headline: '',
            description: 'Jag brukar peppa mig själv genom att…',
            questions: [
              ''
            ]
          },
          {
            id: 8,
            headline: '',
            description: 'Idag är jag mest nöjd med att…',
            questions: [
              ''
            ]
          },
          {
            id: 9,
            headline: '',
            description: 'Jag blir glad när…',
            questions: [
              ''
            ]
          },
          {
            id: 10,
            headline: '',
            description: 'En bra kompis är…',
            questions: [
              ''
            ]
          },
          {
            id: 11,
            headline: '',
            description: 'För att alla ska känna sig välkomna i gruppen är det viktigt att…',
            questions: [
              ''
            ]
          },
          {
            id: 12,
            headline: '',
            description: 'För att göra andra glada brukar jag…',
            questions: [
              ''
            ]
          },
          {
            id: 13,
            headline: '',
            description: 'Det roligaste med konståkning är…',
            questions: [
              ''
            ]
          },
          {
            id: 14,
            headline: '',
            description: 'Jag brukar peppa andra genom att…',
            questions: [
              ''
            ]
          },
          {
            id: 15,
            headline: '',
            description: 'Det bästa med att komma till ishallen är…',
            questions: [
              ''
            ]
          },
          {
            id: 16,
            headline: '',
            description: 'I omklädningsrummet ska man…',
            questions: [
              ''
            ]
          },
          {
            id: 17,
            headline: '',
            description: 'På isen är det viktigt att…',
            questions: [
              ''
            ]
          },
          {
            id: 18,
            headline: '',
            description: 'Det är viktigt att de vuxna i ishallen…',
            questions: [
              ''
            ]
          }
        ]
      }
    ];

    this.contentLoad = new BehaviorSubject(this.content);
  }

  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  shuffleQuestions(categoryId: number){
    const category = this.content.findIndex( category => +category['id'] === +categoryId );
    this.content[category]['question_page'] = this.shuffle(this.content[category]['question_page']);
  }
}
