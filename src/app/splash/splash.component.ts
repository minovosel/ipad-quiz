import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent{

  constructor(
    private router: Router,
  ) { }

  navigateToCategoryGrid(){
    this.router.navigate(['categories']);
  }

}
