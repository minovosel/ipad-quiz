import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SplashComponent } from './splash/splash.component';
import { GridComponent } from './grid/grid.component';
import { QuestionComponent } from './question/question.component';
import { InfoComponent } from './info/info.component';
import { QuestionPrevComponent } from './question/question-prev.component';

const appRoutes: Routes = [
  
    { 
      path: '', 
      component: SplashComponent, 
      pathMatch: 'full'
    },
    { 
      path: 'categories', 
      component: GridComponent,
      data: { state: 'categories' }
    },
    { 
      path: 'category/:id/question/:position', 
      component: QuestionComponent,
      data: { state: 'category/:id/question/:position' }
    },
    { 
      path: 'category/:id/question/:position/prev', 
      component: QuestionPrevComponent,
      data: { state: 'category/:id/question/:position/prev' }
    },
    {
        path: 'info',
        component: InfoComponent,
        data: { state: 'categories' }
    },
    { path: '**', component: SplashComponent }
  ];
  
  @NgModule({
    imports: [
      RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
  })
  
  export class AppRoutingModule {}