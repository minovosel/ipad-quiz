import { Injectable } from '@angular/core';

@Injectable()
export class AnimationsService {
  constructor() {}

  private static routeAnimationType: RouteAnimationType = 'FADE';

  static isRouteAnimationsType(type: RouteAnimationType) {
    return AnimationsService.routeAnimationType === type;
  }

  updateRouteAnimationType(animationType: RouteAnimationType) {
    AnimationsService.routeAnimationType = animationType;
  }
}

export type RouteAnimationType = 'SLIDE' | 'FADE' | 'NONE' | 'SLIDE_REVERT';