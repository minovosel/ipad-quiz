import {trigger, animate, style, group, animateChild, query, stagger, transition} from '@angular/animations';
import { AnimationsService } from './router.animation.service';

const slideTransition = [
    /* order */
    /* 1 */ query(':enter, :leave', style({ position: 'fixed', width:'100%' })
      , { optional: true }),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateX(0%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ], { optional: true }),
    ])
  ];

const slideTransitionRevert = [
    /* order */
    /* 1 */ query(':enter, :leave', style({ position: 'fixed', width:'100%' })
      , { optional: true }),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({ transform: 'translateX(0%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(100%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
      ], { optional: true }),
    ])
  ];

const fadeTransition = [
  query(':enter', 
    [
        style({ opacity: 0 })
    ], 
    { optional: true }
  ),

  query(':leave', 
    [
        style({ opacity: 1 }),
        animate('0.5s', style({ opacity: 0 }))
    ], 
    { optional: true }
  ),

  query(':enter', 
    [
        style({ opacity: 0 }),
        animate('0.5s', style({ opacity: 1 }))
    ], 
    { optional: true }
  )
];

const noTransition = [];

export const routerTransition = trigger('routerTransition', [
  transition( isRouteAnimationsSlide, slideTransition),
  transition(isRouteAnimationsNone, noTransition),
  transition(isRouteAnimationsFade, fadeTransition),
  transition(isRouteAnimationsSlideRevert, slideTransitionRevert),
]);

export function isRouteAnimationsNone() {
  return AnimationsService.isRouteAnimationsType('NONE');
}

export function isRouteAnimationsSlide() {
  return AnimationsService.isRouteAnimationsType('SLIDE');
}

export function isRouteAnimationsFade() {
  return AnimationsService.isRouteAnimationsType('FADE');
}

export function isRouteAnimationsSlideRevert() {
  return AnimationsService.isRouteAnimationsType('SLIDE_REVERT');
}
