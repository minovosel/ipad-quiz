import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { AppMainService } from '../app-main.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent {
  category: object;
  categories: object;
  id: number;
  visibleId: number;
  fade: boolean;

  private swipeCoord?: [number, number];
  private swipeTime?: number;

  constructor(
    private categoryContent: AppMainService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.fade = false;
    this.category = [];
    this.categoryContent.contentLoad
      .subscribe(
        result => this.categories = result
      );
      this.activatedRoute.fragment.subscribe((fragment: string) => {
          if (fragment) {
            this.openInfo(fragment);
          }
      });
  }

  openInfo(id: any) {
    this.id = id;
    this.categoryContent.contentLoad
        .subscribe(
          (result: object[]) => {
           this.category = result.find( category => +category['id'] === +this.id );
           setTimeout(() => {
            this.visibleId = id;
           }, 250);
            setTimeout(() => {
              this.fade = true;
                window.history.replaceState({}, '',`/categories#${id}`);
            }, 500);
              setTimeout(() => {
                  this.fade = false;
              }, 6500);
          }
        )
    this.categoryContent.shuffleQuestions(this.id);
  }

    closeInfo() {
        this.id = null;
        setTimeout(() => {
            this.visibleId = null;
            window.history.replaceState({}, '',`/categories`);
        }, 1);
    }

  swipe(e: TouchEvent, when: string): void {
    const coord: [number, number] = [e.changedTouches[0].pageX, e.changedTouches[0].pageY];
    const time = new Date().getTime();

    if (when === 'start') {
      this.swipeCoord = coord;
      this.swipeTime = time;
    } else if (when === 'end') {
      const direction = [coord[0] - this.swipeCoord[0], coord[1] - this.swipeCoord[1]];
      const duration = time - this.swipeTime;

      if (duration < 1000 //
        && Math.abs(direction[0]) > 30 // Long enough
        && Math.abs(direction[0]) > Math.abs(direction[1] * 3)) { // Horizontal enough
          const swipe = direction[0] < 0 ? 'next' : 'previous';

          this.router.navigate(['/category/' + this.id + '/question/1']);
      }
    }
  }
}
